using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

var services = new ServiceCollection();
services.AddTransient<GetsRoutesValueHelper>();
services.AddSingleton<InMemoryRepository>();

var serviceProvides = services.BuildServiceProvider();

Host.CreateDefaultBuilder(args)
    .ConfigureWebHostDefaults(webBuilder =>
    {
        webBuilder.Configure(app =>
        {
            app.UseRouting();
            
            app.Use(async (context, next) =>
            {
                await next.Invoke();
                
                context.Request.EnableBuffering();
                Console.WriteLine($"Http Request Information:{Environment.NewLine}" +
                                  $"Schema:{context.Request.Scheme} " +
                                  $"Host: {context.Request.Host} " +
                                  $"Path: {context.Request.Path} " +
                                  $"QueryString: {context.Request.QueryString} ");
            });

            app.Run(async (context) =>
            {
                await context.Response.WriteAsync("Hello .NET");
            });

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapGet("/artists/{id:int}", async context =>
                {
                    using var scope = serviceProvides.CreateScope();
                    var repository = scope.ServiceProvider.GetRequiredService<InMemoryRepository>();
                    var routeHelper = scope.ServiceProvider.GetRequiredService<GetsRoutesValueHelper>();
                        
                    var id = routeHelper.Get<int>(context, "id");

                    var artist = repository.Get(id);
                    
                    await context.Response.WriteAsJsonAsync(artist);
                });

                endpoints.MapPost("/artists", async context =>
                {
                    var artist = await context.Request.ReadFromJsonAsync<Artist>();
                    
                    using var scope = serviceProvides.CreateScope();
                    var repository = scope.ServiceProvider.GetRequiredService<InMemoryRepository>();
                    
                    repository.Add(artist);

                    await context.Response.WriteAsJsonAsync(artist);
                });

                endpoints.MapDelete("/artists/{id:int}", async context =>
                {
                    using var scope = serviceProvides.CreateScope();
                    var repository = scope.ServiceProvider.GetRequiredService<InMemoryRepository>();
                    var routeHelper = scope.ServiceProvider.GetRequiredService<GetsRoutesValueHelper>();
                        
                    var id = routeHelper.Get<int>(context, "id");
                    
                    repository.Delete(id);

                    await context.Response.WriteAsJsonAsync(new {status = "ok"});
                });
            });
        });
    }).Build().Run();

record Artist(int Id, string Name);

class InMemoryRepository
{
    private readonly List<Artist> _artists = new();

    public void Add(Artist artist) => _artists.Add(artist);
    public IEnumerable<Artist> GetAll() => _artists;
    public Artist Get(int id) => _artists.FirstOrDefault(x => x.Id == id);
    public void Delete(int id) => _artists.Remove(Get(id));
    public void Update(Artist artist)
    {
        Delete(artist.Id);
        Add(artist);
    }
}

class GetsRoutesValueHelper
{
    public T Get<T>(HttpContext context, string paramName) where T : struct
    {
        var value = context.Request.RouteValues[paramName].ToString();
        if (string.IsNullOrWhiteSpace(value)) throw new ArgumentNullException(nameof(paramName));
        return (T)Convert.ChangeType(value, typeof(T));
    }
}