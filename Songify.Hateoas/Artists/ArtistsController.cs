﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Routing;
using Songify.Hateoas.Artists.Repository;
using Songify.Hateoas.Common;
using Songify.Hateoas.Controllers;
using Songify.Hateoas.Helpers;

namespace Songify.Hateoas.Artists
{
    [ApiController]
    [Route("[controller]")]
    public class ArtistsController : ControllerBase
    {
        private readonly IArtistsRepository _repository;
        private readonly LinkGenerator _linkGenerator;

        public ArtistsController(IArtistsRepository repository, LinkGenerator linkGenerator)
        {
            _repository = repository;
            _linkGenerator = linkGenerator;
        }

        [HttpPost]
        public IActionResult Create([FromBody] Artist artist)
        {
            _repository.Add(artist);

            var shapedArtist = artist.ShapeData(string.Empty);
            ((IDictionary<string, object>)shapedArtist)
                .Add("links", CreateLinksForArtist(artist.Id));

            return CreatedAtRoute(nameof(GetSingle), new {Id = artist.Id}, shapedArtist);
        }

        [HttpGet(Name = nameof(Get))]
        public IActionResult Get([FromQuery] string fields)
        {
            var artists = _repository.GetAll();
            
            var shapedArtistsWithLinks = artists.ShapeData(fields)
                .Select(artist =>
            {
                var artistDictionary = artist as IDictionary<string, object>;
                var authorsLinks = CreateLinksForArtist((int) artistDictionary["Id"]);
                artistDictionary.Add("links", authorsLinks);
                return artistDictionary;
            });

            var links = CreateLinksForArtists();

            var linkedCollectionResource = new
            {
                value = shapedArtistsWithLinks,
                links
            };
            
            return Ok(linkedCollectionResource);
        }

        [HttpPut]
        public ActionResult Update(Artist request)
        {
            _repository.Update(request);
            
            return Ok();
        }

        [HttpDelete]
        [Route("{id}")]
        public ActionResult Delete(int id)
        {
            _repository.Delete(id);
            return NoContent();
        }

        [HttpGet]
        [Route("{id}", Name = nameof(GetSingle))]
        public ActionResult GetSingle(int id, [FromQuery] string fields)
        {
            var artist = _repository.Get(id);
            var result = artist.ShapeData(fields);
            var links = CreateLinksForArtist(artist.Id);
            
            ((IDictionary<string, object>)result)
                .Add("links", links);
            
            return Ok(result);
        }

        private IEnumerable<LinkDto> CreateLinksForArtist(int id)
        {
            var links = new List<LinkDto>
            {
                new LinkDto(_linkGenerator.GetUriByAction(HttpContext, nameof(GetSingle), values: new {id}),
                    "self",
                    "GET"),
                new LinkDto(_linkGenerator.GetUriByAction(HttpContext, nameof(Delete), values: new {id}),
                    "delete",
                    "DELETE"),
                new LinkDto(_linkGenerator.GetUriByAction(HttpContext, nameof(Update), values: new {id}),
                    "update",
                    "PUT")
            };

            return links;
        }

        public bool HasNextPage { get; set; }
        public bool HasPrevPage { get; set; }
        
        private IEnumerable<LinkDto> CreateLinksForArtists()
        {
            var links = new List<LinkDto>
            {
                new LinkDto(_linkGenerator.GetUriByAction(HttpContext, nameof(Get)),
                    "self",
                    "GET")
            };

            if (HasNextPage)
            {
                links.Add(new LinkDto(_linkGenerator.GetUriByAction(HttpContext, nameof(Get)),
                    "self",
                    "GET"));
            }

            return links;
        }
    }
}