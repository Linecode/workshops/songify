using System.Collections.Generic;
using Songify.Hateoas.Controllers;

namespace Songify.Hateoas.Artists.Repository
{
    public interface IArtistsRepository
    {
        void Add(Artist artist);
        IEnumerable<Artist> GetAll();
        Artist Get(int id);
        void Delete(int id);
        void Update(Artist artist);
    }
}