using System.Collections.Generic;
using System.Linq;
using Songify.Hateoas.Controllers;

namespace Songify.Hateoas.Artists.Repository
{
    public class ArtistsInMemoryRepository : IArtistsRepository
    {
        private readonly List<Artist> _artists;

        public ArtistsInMemoryRepository()
        {
            _artists = new List<Artist>();
        }

        public void Add(Artist artist)
        {
            _artists.Add(artist);
        }

        public IEnumerable<Artist> GetAll()
        {
            return _artists;
        }

        public Artist Get(int id)
        {
            return _artists.FirstOrDefault(x => x.Id == id);
        }

        public void Delete(int id)
        {
            _artists.Remove(Get(id));
        }

        public void Update(Artist artist)
        {
            Delete(artist.Id);
            Add(artist);
        }
    }
}