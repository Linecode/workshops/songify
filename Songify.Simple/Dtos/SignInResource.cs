namespace Songify.Simple.Dtos
{
    public class SignInResource
    {
        public string Email { get; set; }
        public string Password { get; set; }
    }
}