using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Songify.Simple.Dtos
{
    public class CreateArtistResource : IValidatableObject
    {
        [Required]
        [MaxLength(300)]
        public string Name { get; set; }
        
        public string Origin { get; set; }
        public DateTime? CreatedAt { get; set; }
        public bool? IsActive { get; set; }
        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            if (CreatedAt > DateTime.UtcNow)
            {
                yield return new ValidationResult("The Created At should not be in future", 
                    new [] { nameof(CreatedAt) });
            }
        }
    }
}
