using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Songify.Persistence;
using Songify.Simple.Dtos.ResourceParameters;
using Songify.Simple.Helpers;
using Songify.Simple.Models;

namespace Songify.Simple.DAL
{
    public interface IArtistsRepository
    {
        IUnitOfWork UnitOfWork { get; }
        void Add(Artist model);
        Task<Artist> Get(int id);
        void Update(Artist artist);
        void Remove(int id);
        Task<PagedList<Artist>> GetArtists(ArtistResourceParameters artistResourceParameters);
    }

    public class ArtistsRepository : IRepository, IArtistsRepository
    {
        private readonly SongifyDbContext _context;

        public IUnitOfWork UnitOfWork => _context;
        
        public ArtistsRepository(SongifyDbContext context)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
        }

        public void Add(Artist model)
        {
            _context.Artists.Add(model);
        }

        public Task<Artist> Get(int id)
        {
            return _context.Artists
                .FirstOrDefaultAsync(x => x.Id == id);
        }

        public void Update(Artist artist)
        {
            _context.Entry(artist).State = EntityState.Modified;
        }

        public void Remove(int id)
        {
            _context.Artists.Remove(new Artist {Id = id});
        }

        public Task<PagedList<Artist>> GetArtists(ArtistResourceParameters artistResourceParameters)
        {
            var collection = _context.Artists.AsQueryable();

            if (artistResourceParameters.IsActive.HasValue)
            {
                collection = collection.Where(x => x.IsActive == artistResourceParameters.IsActive);
            }

            if (!string.IsNullOrWhiteSpace(artistResourceParameters.SearchQuery))
            {
                var searchQuery = artistResourceParameters.SearchQuery.Trim();
                collection = collection.Where(x => x.Name.Contains(searchQuery) || x.Origin.Contains(searchQuery));
            }
            
            return PagedList<Artist>.Create(collection, artistResourceParameters.PageNumber, artistResourceParameters.PageSize);
        }
    }
}