using Marvin.Cache.Headers;
using Microsoft.AspNetCore.Mvc;
using Songify.Simple.Dtos;
using Songify.Simple.Services.IdentityService;

namespace Songify.Simple.Controllers
{
    [ApiController]
    public class IdentityController : ControllerBase
    {
        private readonly JwtHandler _jwtHandler;

        public IdentityController(JwtHandler jwtHandler)
        {
            _jwtHandler = jwtHandler;
        }

        /// <summary>
        /// This is a summary of a method
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        [HttpCacheValidation(NoCache = true)]
        [Route("api/[controller]/[action]")]
        [ActionName("sing-in")]
        public IActionResult Index(SignInResource request)
        {
            var token = _jwtHandler.CreateToken(1, request.Email);

            return Ok(token);
        }
    }
}