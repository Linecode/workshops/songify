using System.Text.Json;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Songify.Simple.DAL;
using Songify.Simple.Dtos;
using Songify.Simple.Dtos.ResourceParameters;
using Songify.Simple.Helpers;
using Songify.Simple.Models;

namespace Songify.Simple.Controllers
{
    [ApiController]
    [Route("api/[controller]/")]
    [Consumes(System.Net.Mime.MediaTypeNames.Application.Json)]
    public class ArtistsController : ControllerBase
    {
        private readonly IArtistsRepository _repository;
        private readonly IMapper _mapper;

        public ArtistsController(IArtistsRepository repository, IMapper mapper)
        {
            _repository = repository;
            _mapper = mapper;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> Create(CreateArtistResource request)
        {
            var artist = _mapper.Map<CreateArtistResource, Artist>(request);
            
            _repository.Add(artist);
            await _repository.UnitOfWork.SaveChangesAsync();

            return Created($"api/artists/{artist.Id}", request);
        }

        /// <summary>
        /// Get a specific Artist 
        /// </summary>
        /// <remarks>
        /// Some more comment inside remarks tag
        /// </remarks>
        /// <param name="id">Artist's id</param>
        /// <returns>An Artist Entity</returns>
        /// <response code="200">Returns existing artists entity</response>
        [HttpGet]
        [HttpHead]
        [Route("{id}")]
        [Produces(System.Net.Mime.MediaTypeNames.Application.Json)]
        [ProducesResponseType(typeof(Artist), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> Get(int id)
        {
            var artist = await _repository.Get(id);

            if (artist is null)
                return NotFound();
            
            return Ok(artist);
        }

        /// <summary>
        /// Get a list of Artist 
        /// </summary>
        /// <remarks>
        /// Some more comment inside remarks tag
        /// </remarks>
        /// <returns>List of artists</returns>
        /// <response code="200">Returns existing artists entity</response>
        [HttpGet]
        [HttpHead]
        [Produces(System.Net.Mime.MediaTypeNames.Application.Json)]
        [ProducesResponseType(typeof(PagedList<Artist>), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> GetListOfArtists([FromQuery] ArtistResourceParameters artistResourceParameters)
        {
            var artists = await _repository.GetArtists(artistResourceParameters);

            var paginationMetadata = new
            {
                totalCount = artists.TotalCount,
                totalPages = artists.TotalPages,
                currentPage = artists.CurrentPage,
                pageSize = artists.PageSize
            };
            
            Response.Headers.Add("X-Pagination", JsonSerializer.Serialize(paginationMetadata));
            
            return Ok(artists);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Authorize]
        [HttpDelete]
        [Route("{id}")]
        public async Task<IActionResult> Remove(int id)
        {
            _repository.Remove(id);
            await _repository.UnitOfWork.SaveChangesAsync();

            // https://restfulapi.net/http-methods/#delete
            return NoContent();
        }
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPut]
        public async Task<IActionResult> Update(UpdateArtistResource request)
        {
            var artist = await _repository.Get(request.Id);

            if (artist is null)
                return NotFound();

            _mapper.Map(request, artist);
            
            _repository.Update(artist);
            await _repository.UnitOfWork.SaveChangesAsync();

            return Ok();
        }
    }
}