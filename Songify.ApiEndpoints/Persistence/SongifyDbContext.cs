using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Songify.Albums.Model;
using Songify.Artists.Model;

namespace Songify.Persistence
{
    public class SongifyDbContext : DbContext, IUnitOfWork
    {
        public DbSet<Artist> Artists { get; set; }
        public DbSet<Album> Albums { get; set; }

        public SongifyDbContext(DbContextOptions<SongifyDbContext> options) : base(options)
        {
        }
        
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        { 
            optionsBuilder.UseLoggerFactory(MyLoggerFactory);
        }
        
        private static readonly ILoggerFactory MyLoggerFactory = LoggerFactory.Create(builder => { builder.AddConsole(); });
    }
}