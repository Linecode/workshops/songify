using System;
using System.Threading;
using System.Threading.Tasks;
using Ardalis.ApiEndpoints;
using Microsoft.AspNetCore.Mvc;
using Songify.Albums.Model;
using Songify.Albums.Repository;
using Songify.Albums.Resources;
using Swashbuckle.AspNetCore.Annotations;

namespace Songify.Albums
{
    public class Create : BaseAsyncEndpoint
        .WithRequest<CreateAlbumResource>
        .WithoutResponse
    {
        private readonly IAlbumRepository _repository;

        public Create(IAlbumRepository repository)
        {
            _repository = repository;
        }
        
        [HttpPost("api/artist/{artistId}/album", Name = RouteName)]
        [SwaggerOperation(
            Summary = "Create a specific Album",
            Description = "Create a specific Album",
            OperationId = "Album.Create",
            Tags = new []{ "AlbumsEndpoint"})]
        public override async Task<ActionResult> HandleAsync(CreateAlbumResource request, CancellationToken cancellationToken = new CancellationToken())
        {
            // We can grab this value from RouteValues dictionary or to merge it with JsonPathDocument
            var id = Convert.ToInt32(HttpContext.Request.RouteValues["artistId"]?.ToString());

            var album = new Album
            {
                Name = request.Name,
                ArtistId = id
            };

            _repository.Add(album);
            await _repository.UnitOfWork.SaveChangesAsync(cancellationToken);

            return CreatedAtRoute(Get.RouteName, new {id = album.Id}, album);
        }

        public const string RouteName = "Albums.Create";
    }
}