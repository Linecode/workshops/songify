using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Songify.Albums.Model;
using Songify.Artists.Model;
using Songify.Persistence;

namespace Songify.Albums.Repository
{
    public interface IAlbumRepository
    {
        IUnitOfWork UnitOfWork { get; }
        void Add(Album model);
        Task<Album> Get(int id);
        void Update(Album album);
        void Remove(int id);
        Task<List<Album>> GetAllFromArtist(int artistId);
    }

    public class AlbumRepository : IRepository, IAlbumRepository
    {
        private readonly SongifyDbContext _context;

        public IUnitOfWork UnitOfWork => _context;
        
        public AlbumRepository(SongifyDbContext context)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
        }

        public void Add(Album model)
        {
            _context.Albums.Add(model);
        }

        public Task<Album> Get(int id)
        {
            return _context.Albums
                .FirstAsync(x => x.Id == id);
        }

        public void Update(Album album)
        {
            _context.Entry(album).State = EntityState.Modified;
        }

        public void Remove(int id)
        {
            _context.Albums.Remove(new Album {Id = id});
        }

        public Task<List<Album>> GetAllFromArtist(int artistId)
        {
            return _context.Albums.Where(x => x.ArtistId == artistId)
                .ToListAsync();
        }
    }
}