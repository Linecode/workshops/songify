namespace Songify.Albums.Resources
{
    public class CreateAlbumResource
    {
        public string Name { get; set; }
    }
}