using System.Threading;
using System.Threading.Tasks;
using Ardalis.ApiEndpoints;
using Microsoft.AspNetCore.Mvc;
using Songify.Albums.Repository;
using Swashbuckle.AspNetCore.Annotations;

namespace Songify.Albums
{
    public class GetAllFromArtist : BaseAsyncEndpoint
        .WithRequest<int>
        .WithoutResponse
    {
        private readonly IAlbumRepository _repository;

        public GetAllFromArtist(IAlbumRepository repository)
        {
            _repository = repository;
        }
        
        [HttpGet("api/artists/{artistId}/albums", Name = RouteName)]
        [SwaggerOperation(
            Summary = "Get All Albums from a specific Artist",
            Description = "Get All Albums from a specific Artist",
            OperationId = "Album.GetAllFromArtists",
            Tags = new []{ "AlbumsEndpoint"})]
        public override async Task<ActionResult> HandleAsync(int artistId, CancellationToken cancellationToken = new ())
        {
            var albums = await _repository.GetAllFromArtist(artistId);

            return Ok(albums);
        }

        public const string RouteName = "Albums.GetAllFromArtist";
    }
}