using System.Threading;
using System.Threading.Tasks;
using Ardalis.ApiEndpoints;
using Microsoft.AspNetCore.Mvc;
using Songify.Albums.Repository;
using Swashbuckle.AspNetCore.Annotations;

namespace Songify.Albums
{
    public class Get : BaseAsyncEndpoint
        .WithRequest<int>
        .WithoutResponse
    {
        private readonly IAlbumRepository _repository;

        public Get(IAlbumRepository repository)
        {
            _repository = repository;
        }
        
        [HttpGet("api/albums/{id}", Name = RouteName)]
        [SwaggerOperation(
            Summary = "Get a specific Album",
            Description = "Get a specific Album",
            OperationId = "Album.Get",
            Tags = new []{ "AlbumsEndpoint"})]
        public override async Task<ActionResult> HandleAsync(int id, CancellationToken cancellationToken = new CancellationToken())
        {
            var album = await _repository.Get(id);

            if (album is null)
                return BadRequest();

            return Ok(album);
        }
        
        public const string RouteName = "Albums.Get";
    }
}