using System.Threading;
using System.Threading.Tasks;
using Ardalis.ApiEndpoints;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Songify.Artists.Repository;
using Swashbuckle.AspNetCore.Annotations;

namespace Songify.Artists
{
    public class Delete : BaseAsyncEndpoint
        .WithRequest<int>
        .WithoutResponse
    {
        private readonly IArtistsRepository _repository;

        public Delete(IArtistsRepository repository)
        {
            _repository = repository;
        }
        
        [Authorize]
        [HttpDelete("/api/artists/{id}")]
        [SwaggerOperation(
            Summary = "Delete a specific Artist",
            Description = "Delete a specific Artist",
            OperationId = "Artist.Delete",
            Tags = new []{ "ArtistsEndpoint" })
        ]
        public override async Task<ActionResult> HandleAsync(int id, CancellationToken cancellationToken = new ())
        {
            _repository.Remove(id);
            await _repository.UnitOfWork.SaveChangesAsync(cancellationToken);

            // https://restfulapi.net/http-methods/#delete
            return NoContent();
        }
    }
}