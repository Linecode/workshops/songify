namespace Songify.Artists.Resources
{
    public class UpdateArtistResource : CreateArtistResource
    {
        public int Id { get; set; }
    }
}