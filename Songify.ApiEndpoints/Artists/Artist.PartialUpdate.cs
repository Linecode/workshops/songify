using System;
using System.Threading;
using System.Threading.Tasks;
using Ardalis.ApiEndpoints;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;
using Songify.Artists.Model;
using Songify.Artists.Repository;
using Swashbuckle.AspNetCore.Annotations;

namespace Songify.Artists
{
    public class PartialUpdate : BaseAsyncEndpoint
        .WithRequest<JsonPatchDocument<Artist>>
        .WithoutResponse
    {
        private readonly IArtistsRepository _repository;

        public PartialUpdate(IArtistsRepository repository)
        {
            _repository = repository;
        }
        
        [HttpPatch("/api/artists/{id}")]
        [SwaggerOperation(
            Summary = "Partial Update a specific Artist",
            Description = "Partial Update a specific Artist",
            OperationId = "Artist.PartialUpdate",
            Tags = new []{ "ArtistsEndpoint" })
        ]
        public override async Task<ActionResult> HandleAsync(JsonPatchDocument<Artist> request, CancellationToken cancellationToken = new ())
        {
            // We can grab this value from RouteValues dictionary or to merge it with JsonPathDocument
            var id = Convert.ToInt32(HttpContext.Request.RouteValues["id"]?.ToString());

            var artist = await _repository.Get(id);

            if (artist is null) return NotFound();
            
            request.ApplyTo(artist, ModelState);

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            
            _repository.Update(artist);
            await _repository.UnitOfWork.SaveChangesAsync(cancellationToken);

            return Ok();
        }
    }
}