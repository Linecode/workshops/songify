using System.Threading;
using System.Threading.Tasks;
using Ardalis.ApiEndpoints;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Songify.Artists.Model;
using Songify.Artists.Repository;
using Songify.Artists.Resources;
using Swashbuckle.AspNetCore.Annotations;

namespace Songify.Artists
{
    public class Create : BaseAsyncEndpoint
        .WithRequest<CreateArtistResource>
        .WithoutResponse
    {
        private readonly IArtistsRepository _artistsRepository;
        private readonly IMapper _mapper;

        public Create(IArtistsRepository artistsRepository, IMapper mapper)
        {
            _artistsRepository = artistsRepository;
            _mapper = mapper;
        }

        [HttpPost("api/artists")]
        [SwaggerOperation(
            Summary = "Create a specific Artist",
            Description = "Create a specific Artist",
            OperationId = "Artist.Create",
            Tags = new []{ "ArtistsEndpoint"})]
        public override async Task<ActionResult> HandleAsync(CreateArtistResource request, 
            CancellationToken cancellationToken = new ())
        {
            var artist = _mapper.Map<CreateArtistResource, Artist>(request);
            
            _artistsRepository.Add(artist);
            await _artistsRepository.UnitOfWork.SaveChangesAsync(cancellationToken);

            return Created($"api/artists/{artist.Id}", request);
        }
    }
}