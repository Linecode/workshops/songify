using System.Threading;
using System.Threading.Tasks;
using Ardalis.ApiEndpoints;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Songify.Artists.Repository;
using Songify.Artists.Resources;
using Swashbuckle.AspNetCore.Annotations;

namespace Songify.Artists
{
    public class Update : BaseAsyncEndpoint
        .WithRequest<UpdateArtistResource>
        .WithoutResponse
    {
        private readonly IArtistsRepository _artistsRepository;
        private readonly IMapper _mapper;

        public Update(IArtistsRepository artistsRepository, IMapper mapper)
        {
            _artistsRepository = artistsRepository;
            _mapper = mapper;
        }
        
        [HttpPut("api/artists/")]
        [SwaggerOperation(
            Summary = "Update a specific Artist",
            Description = "Update a specific Artist",
            OperationId = "Artist.Update",
            Tags = new []{ "ArtistsEndpoint" })
        ]
        public override async Task<ActionResult> HandleAsync(UpdateArtistResource request, CancellationToken cancellationToken = new CancellationToken())
        {
            var artist = await _artistsRepository.Get(request.Id);

            if (artist is null)
                return NotFound();

            _mapper.Map(request, artist);
            
            _artistsRepository.Update(artist);
            await _artistsRepository.UnitOfWork.SaveChangesAsync(cancellationToken);

            return Ok();
        }
    }
}