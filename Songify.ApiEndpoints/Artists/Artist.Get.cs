using System.Threading;
using System.Threading.Tasks;
using Ardalis.ApiEndpoints;
using Marvin.Cache.Headers;
using Microsoft.AspNetCore.Mvc;
using Songify.Artists.Model;
using Songify.Artists.Repository;
using Swashbuckle.AspNetCore.Annotations;

namespace Songify.Artists
{
    public class Get : BaseAsyncEndpoint
        .WithRequest<int>
        .WithResponse<Artist>
    {
        private readonly IArtistsRepository _artistsRepository;

        public Get(IArtistsRepository artistsRepository)
        {
            _artistsRepository = artistsRepository;
        }
        
        [HttpGet("/api/artists/{id}")]
        [HttpCacheExpiration(CacheLocation = CacheLocation.Public, MaxAge = 600)]
        [HttpCacheValidation]
        [SwaggerOperation(
            Summary = "Get a specific Artist",
            Description = "Get a specific Artist",
            OperationId = "Artist.Get",
            Tags = new []{ "ArtistsEndpoint" })
        ]
        public override async Task<ActionResult<Artist>> HandleAsync(int id, CancellationToken cancellationToken = new())
        {
            var artist = await _artistsRepository.Get(id);

            if (artist is null)
                return NotFound();
            
            return Ok(artist);
        }
    }
}