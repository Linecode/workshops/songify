using AutoMapper;
using Songify.Artists.Model;
using Songify.Artists.Resources;

namespace Songify.Artists
{
    public class ArtistsProfiles : Profile
    {
        public ArtistsProfiles()
        {
            CreateMap<CreateArtistResource, Artist>();
            CreateMap<UpdateArtistResource, Artist>();

            CreateMap<Artist, CreateArtistResource>();
            CreateMap<Artist, UpdateArtistResource>();
        }
    }
}