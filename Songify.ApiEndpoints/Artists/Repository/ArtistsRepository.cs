using System;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Songify.Artists.Model;
using Songify.Persistence;

namespace Songify.Artists.Repository
{
    public interface IArtistsRepository
    {
        IUnitOfWork UnitOfWork { get; }
        void Add(Artist model);
        Task<Artist> Get(int id);
        void Update(Artist artist);
        void Remove(int id);
    }

    public class ArtistsRepository : IRepository, IArtistsRepository
    {
        private readonly SongifyDbContext _context;

        public IUnitOfWork UnitOfWork => _context;
        
        public ArtistsRepository(SongifyDbContext context)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
        }

        public void Add(Artist model)
        {
            _context.Artists.Add(model);
        }

        public Task<Artist> Get(int id)
        {
            return _context.Artists
                .FirstAsync(x => x.Id == id);
        }

        public void Update(Artist artist)
        {
            _context.Entry(artist).State = EntityState.Modified;
        }

        public void Remove(int id)
        {
            _context.Artists.Remove(new Artist {Id = id});
        }
    }
}