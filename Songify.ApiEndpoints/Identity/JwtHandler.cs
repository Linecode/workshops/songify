using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using Microsoft.IdentityModel.Tokens;
using Songify.Utils;

namespace Songify.Identity
{
    public class JwtHandler
    {
        private readonly SigningCredentials _signingCredentials;

        public JwtHandler()
        {
            var issuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes("JLBMU2VbJZmt42sUwByUpJJF6Y5mG2gPNU9sQFUpJFcGFJdyKxskR3bxh527kax2UcXHvB"));
            _signingCredentials = new SigningCredentials(issuerSigningKey, SecurityAlgorithms.HmacSha256);
        }

        public JsonWebToken CreateToken(int userId, string email, IDictionary<string, string> claims = null, string refreshToken = "")
        {
            var now = DateTime.UtcNow;
            var jwtClaims = new List<Claim>
            {
                new Claim(JwtRegisteredClaimNames.Sub, userId.ToString()),
                new Claim(JwtRegisteredClaimNames.UniqueName, email),
                new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                new Claim(JwtRegisteredClaimNames.Iat, now.ToTimestamp().ToString()),
            };

            var customClaims = claims?.Select(claim => new Claim(claim.Key, claim.Value)).ToArray()
                               ?? Array.Empty<Claim>();
            jwtClaims.AddRange(customClaims);
            var expires = now.AddMinutes(60);
            var jwt = new JwtSecurityToken(
                issuer: "Songify.Identity",
                claims: jwtClaims,
                notBefore: now,
                expires: expires,
                signingCredentials: _signingCredentials
            );
            var token = new JwtSecurityTokenHandler().WriteToken(jwt);

            return new JsonWebToken
            {
                UserId = userId,
                AccessToken = token,
                RefreshToken = refreshToken,
                Expires = expires.ToTimestamp(),
                Claims = customClaims.ToDictionary(c => c.Type, c => c.Value)
            };
        }
    }
}