using Newtonsoft.Json;

namespace Songify.Identity
{
    public class SignInResource
    {
        [JsonProperty(PropertyName = "email")]
        public string Email { get; set; }
        [JsonProperty(PropertyName = "password")]
        public string Password { get; set; }
    }
}