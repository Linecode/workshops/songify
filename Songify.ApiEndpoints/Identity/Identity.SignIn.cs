using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Ardalis.ApiEndpoints;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;

namespace Songify.Identity
{
    public class SignIn : BaseAsyncEndpoint
        .WithRequest<SignInResource>
        .WithResponse<JsonWebToken>
    {
        private readonly JwtHandler _jwtHandler;

        public SignIn(JwtHandler jwtHandler)
        {
            _jwtHandler = jwtHandler;
        }
        
        [HttpPost("/api/identity/sign-in")]
        [SwaggerOperation(
            Summary = "Create JWT Token",
            Description = "Create JWT Token",
            OperationId = "Identity.SignIn",
            Tags = new []{ "IdentityEndpoints" })
        ]
        public override async Task<ActionResult<JsonWebToken>> HandleAsync(SignInResource request, CancellationToken cancellationToken = new CancellationToken())
        {
            var token = _jwtHandler.CreateToken(1, request.Email);

            return Ok(token);
        }
    }
}