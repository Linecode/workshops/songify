using System.Text.RegularExpressions;
using Microsoft.AspNetCore.Routing;

namespace Songify.Utils
{
    public class SlugifyParameterTransformer : IOutboundParameterTransformer
    {
        public string? TransformOutbound(object? value)
        {
            if (value == null) return null;

            return Regex.Replace(value.ToString() ?? string.Empty, "([a-z])([A-z])", "$1-$2").ToLower();
        }
    }
}